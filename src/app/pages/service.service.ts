import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  baseurl: any;
  constructor(private http: HttpClient) {
    this.baseurl = environment.url;
 }

 create_slot(data)
 {
  return this.http.post(this.baseurl + '/createslot', data,{})
}
get_solt(data)
{
  return this.http.post(this.baseurl + '/getslots', data,{})
}

 
}
