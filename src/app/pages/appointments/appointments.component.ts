import { Component, OnInit, TemplateRef } from '@angular/core';
import { NbComponentStatus, NbDialogService } from '@nebular/theme';
import { ServiceService } from '../service.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
  mrgslot: any;
  eveslot: any;
  date = new Date();
  date2 = new Date();
  slotslist: any;
  slotslist1: any;


  constructor(private service: ServiceService, private toastrService: NbToastrService) {
    this.date2.setDate(this.date2.getDate() - 1);
  }


  ngOnInit(): void {

    this.get_slots(this.date)
  }

  //  create slots
  onModelChanged() {

    let slots = this.mrgslot

    this.slotslist.filter(e => e == slots)
    if (this.slotslist.filter(e => e == slots).length == 0) {
      let Obeject = {
        doc_id: 1,
        doc_name: 'sam',
        date_slot: new Date(this.date).toLocaleDateString("en-US"),
        slots_type: 1,
        slot: slots
      }
      console.log(Obeject)
      this.service.create_slot(Obeject).subscribe((data: any) => {
        if(data.success==true || data.success==false)
        {
          this.get_slots(this.date)
        }

      })
    }
    else {
      this.showToast('danger')

    }

  }
  //  tostr
  showToast(status: NbComponentStatus) {
    let title = "Warning"
    this.toastrService.show(title, `${new Date(this.date).toLocaleDateString("en-US")}- Already Added Duplicated Not Allowod !`, { status });
  }
  onModelChangedEve() {

    let slots = this.eveslot
    if (this.slotslist1.filter(e => e == slots).length == 0) {
      let Obeject = {
        doc_id: 1,
        doc_name: 'sam',
        date_slot: new Date(this.date).toLocaleDateString("en-US"),
        slots_type: 2,
        slot: slots
      }
      console.log(Obeject)
      this.service.create_slot(Obeject).subscribe((data: any) => {
        if(data.success==true || data.success==false )
        {
          this.get_slots(this.date)
        }
      })
    }
    else {
      this.showToast('danger')

    }
  }
  // slot list
  get_slots(change) {

    console.log(change)
    this.slotslist = [];
    this.slotslist1=[];
    let Obeject = {
      doc_id: 1,
      doc_name: 'sam',
      date_slot: new Date(this.date).toLocaleDateString("en-US"),
    }
    this.service.get_solt(Obeject).subscribe((data: any) => {
      console.log(data)
      if (data.data.length != 0) {
        let slots = data.data.filter((e) => e.slot_type == 1)
        this.slotslist=slots.length>0?slots[0].slots:[]
        let slots1 = data.data.filter((e) => e.slot_type == 2)
        
        this.slotslist1=slots1.length>0?slots1[0].slots:[]
      }

    })
  }

}