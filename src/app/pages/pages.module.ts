import { NgModule } from '@angular/core';
import { NbButtonGroupModule, NbButtonModule, NbCalendarModule, NbCardModule, NbIconModule, NbLayoutModule, NbMenuModule, NbTabsetModule, NbTimepickerModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';

import { PagesRoutingModule } from './pages-routing.module';
import { DrHomeComponent } from './dr-home/dr-home.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {FormsModule} from '@angular/forms';
@NgModule({
  imports: [
    FormsModule,
    NbTabsetModule,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbButtonGroupModule,
    NgxMaterialTimepickerModule,
    NbEvaIconsModule,
    NbIconModule,
    NbButtonModule,
    NbCardModule,
    NbTimepickerModule,
    NbCalendarModule,
    NbLayoutModule
   
  ],
  declarations: [
    PagesComponent,
    DrHomeComponent,
    AppointmentsComponent,
  ],
})
export class PagesModule {
}
