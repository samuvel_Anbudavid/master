import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';

import { DrHomeComponent } from './dr-home/dr-home.component';
import { AppointmentsComponent } from './appointments/appointments.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
   
    {
      path: 'dr-dashboard',
      component: DrHomeComponent,
    },
    {
      path: 'appointment',
      component: AppointmentsComponent,
    },
    {
      path: '',
      redirectTo: 'dr-dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: DrHomeComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
